
## Motivation

The goal of the project is to predict valuable data points from [Magnetospheric Multiscale Mission](https://mms.gsfc.nasa.gov/). The mission includes a constellation of four satellites that orbit Earth in a highly elliptical orbit. The elliptical orbit limits the available data bandwidth and allow for only 2%-4% of total recorded data can be downloaded. The satellites have on-board buffer storage which can hold recorded data for a period of about 72 hours; after that the data is lost forever. 

The mission aims to record information about Earth's magnetic field. Since the magnetic field is dynamic and changes continuously (with changing solar activity, for example) it is not possible to determine interesting data points just based on the distance from Earth or another orbital property. Currently, a scientist (a different one every week) on Earth monitors a low-resolution data stream and decides when the detailed data should be download. This scientist is known as a *SITL*: a scientist in the loop.

We will try to come up with a machine-learning method that can replace the SITL and decide when the data being recorded is sufficiently important that it needs to be downloaded.

Slides describing the mission and the prediction problem are [here](SpaceML.pdf). 

## Data Structure

Each dataset consists of two files:

1. *MMS data stream*: Low-resolution data recorded by the satellites that is available to the scientist when deciding on the time intervals for which the detailed data should be downloaded. The file name is `mms_[date].csv`.

2. *SITL selection*: Time intervals selected by the scientist. The file name is `sitl_[date].csv`

### MMS data stream

Each row corresponds to a single data point. Each column corresponds to a feature. The column `Time` specifies when the data point was recorded.

### SITL selection

Each row corresponds to an interval selected by the scientist. The columns are:

- `Start`: Start of the time interval to be transmitted
- `End`: End of the time interval to be transmitted
- `Priority`: Priority of the request. Higher number means higher priority. Does not need to be addressed by the project at this time.
- `Status`: Whether the data interval was dowloaded
- `Reason`: Reason for selecting the particular time interval. Probably useless at this stage.

## Objective

Predict for each data point in the MMS dataset (each row) whether is it included in at least one interval specified in the SITL file. The SITL intervals are understood to be *inclusive* of both the start and the end of the interval.

